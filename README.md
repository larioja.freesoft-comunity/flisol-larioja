# FLISOL-LaRioja

Landing page FLISOL La Rioja


# Pasos para ejecutar el server

## Creacion de Virtual env

Estar seguro de tener instalado venv. Luego ejecutar

```bash
python3 -m venv venv
```
Ingresar al venv de la siguiente manera:

```bash
source venv/bin/activate
```


## Instalar requerimientos

```bash
pip install -r requirements.txt
```

## Ejecutar server

```bash
cd flisol/
python manage.py runserver
```
